//////////////////////////////////////////////////////
//  DroneSpeechROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneSpeechROSModule.h"



using namespace std;




DroneSpeechROSModule::DroneSpeechROSModule() : DroneModule(droneModule::active,1)
{

    return;
}


DroneSpeechROSModule::~DroneSpeechROSModule()
{
	close();
	return;
}


bool DroneSpeechROSModule::init()
{
    DroneModule::init();



    //end
    return true;
}


void DroneSpeechROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    DroneModule::open(nIn);

    //Init
    if(!init())
        cout<<"Error init"<<endl;


    // Get parameters
    ros::param::get("~sound_topic_name", soundTopicName);
    if ( soundTopicName.length() == 0)
    {
        soundTopicName="robotsound";
    }
    std::cout<<"soundTopicName="<<soundTopicName<<std::endl;

    ros::param::get("~message_to_say_topic_name", messageToSayTopicName);
    if ( messageToSayTopicName.length() == 0)
    {
        messageToSayTopicName="message_to_say";
    }
    std::cout<<"messageToSayTopicName="<<messageToSayTopicName<<std::endl;


    // Options
    // voice = "voice_kal_diphone";
    // voice = "voice_rab_diphone";
    // voice = "voice_el_diphone";
    ros::param::get("~voice", voice);
    if ( voice.length() == 0)
    {
        voice="voice_el_diphone";
    }
    std::cout<<"voice="<<voice<<std::endl;

	

    // Publishers
    soundPub = n.advertise<sound_play::SoundRequest>(soundTopicName, 1);


    // Subscribers
    messageToSaySub = n.subscribe(messageToSayTopicName, 1, &DroneSpeechROSModule::messageToSayCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    //moduleStarted=true;


	//End
	return;
}


void DroneSpeechROSModule::messageToSayCallback(const std_msgs::String::ConstPtr & msg)
{
    if(!moduleStarted)
        return;

    soundMsg.arg = msg->data;

    soundPublish();

    return;
}


int DroneSpeechROSModule::soundPublish()
{
    if(!moduleStarted)
        return 0;


    if(soundMsg.arg=="")
    {
        // Fill the message
        soundMsg.sound = sound_play::SoundRequest::ALL;
        soundMsg.command = sound_play::SoundRequest::PLAY_STOP;;

        // Voice
        soundMsg.arg2 = "";
    }
    else
    {
        // Fill the message
        soundMsg.sound = sound_play::SoundRequest::SAY;
        soundMsg.command = sound_play::SoundRequest::PLAY_ONCE;


        // Voice
        soundMsg.arg2 = voice;
    }


    // Publish
    soundPub.publish(soundMsg);


    return 1;
}



void DroneSpeechROSModule::close()
{
    DroneModule::close();




    return;
}


bool DroneSpeechROSModule::resetValues()
{


    return true;
}


bool DroneSpeechROSModule::startVal()
{


    //End
    return DroneModule::startVal();
}


bool DroneSpeechROSModule::stopVal()
{


    return DroneModule::stopVal();
}


bool DroneSpeechROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;



    return true;
}


